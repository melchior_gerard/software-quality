def main():

    #Saisi des chiffres et de l'opérateur
    firstInt = int(input("Veuillez entrer un chiffre \n"))
    operator = input("Veuillez entrer un opérateur \n")
    secondInt = int(input("veuillez choisir un deuxieme chiffre \n"))
    nameFile = input("dans quel fichier voulez vous écrire le resultat ?\n")

    #Calcul en fonction de l'opérateur choisis
    if operator == "+":
        result = firstInt + secondInt
    elif operator =="-":
        result = firstInt - secondInt
    elif operator =="*":
        result = firstInt * secondInt
    elif operator =="/":
        result = firstInt / secondInt

    #Création, écriture et fermeture du fichier
    fileToWrite =  open(nameFile + ".txt", "w")
    fileToWrite.write(str(firstInt) + operator + str(secondInt) + " =" + str(result))
    fileToWrite.close()


if __name__== "__main__":
    main()